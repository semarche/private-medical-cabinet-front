backend part: https://gitlab.com/semarche/private-medical-cabinet-backend

The project is under development

how to run:

git clone https://gitlab.com/semarche/private-medical-cabinet-front.git<br />
cd private-medical-cabinet-front<br />
npm install<br />
npm run dev

or you can use Docker for build image and start app inside container:<br />
docker build -t private-medical-cabinet-front .<br />
docker run -d -p 80:3000 --name pmcf_container private-medical-cabinet-front

or you can use already built Docker image<br />
docker pull registry.gitlab.com/semarche/private-medical-cabinet-front<br />
docker run -d -p 80:3000 --name pmcb_container registry.gitlab.com/semarche/private-medical-cabinet-front

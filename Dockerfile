FROM node:8-alpine
MAINTAINER Marchenko Sergey <sergey.dnepro@gmail.com>

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

# Create app directory
RUN mkdir -p /app
WORKDIR /app

COPY package.json /app/
RUN npm install --no-optional  --silent

ADD ./ /app

RUN npm run build
#RUN npm run eject

EXPOSE 3000

CMD ["npm", "run", "prod"]


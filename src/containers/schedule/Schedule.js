import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Layout, Menu, Icon, Row, Col, Calendar } from 'antd'
import './index.css'


const propTypes = {

}

export class Schedule extends Component {
  constructor(props) {
    super()
  }
  onPanelChange(value, mode) {
    console.log(value, mode)
  }
  render() {
    return (
      <div className='schedule'>
        <Calendar onPanelChange={this.onPanelChange} />
      </div>
      )
  }
}

Schedule.propTypes = propTypes
export default Schedule

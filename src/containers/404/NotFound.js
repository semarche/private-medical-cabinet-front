import React from 'react'
import Exception from 'ant-design-pro/lib/Exception'
import './index.css'

const NotFound = () => <Exception type='404' />

export default NotFound

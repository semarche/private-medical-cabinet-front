import { connect } from 'react-redux'

import Component from './NotFound'

const mapActionCreators = {
}

const mapStateToProps = (state) => ({

})

export default connect(mapStateToProps, mapActionCreators)(Component)

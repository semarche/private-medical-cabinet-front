import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import Login from './Component'
import { logIn } from '../../modules/user'

const form = {
  form: 'creative-form',
  enableReinitialize: true,
  validate: (values, props) => {
    const errors = {}
    if (!props.dirty) {
      return errors
    }
    if (!values.email) {
      errors.name = 'Email is empty'
    }
    if (!values.email) {
      errors.password = 'Password is empty'
    }
    return errors
  },
  onSubmit: (values, dispatch, props) =>
    props.logIn(values.email, values.password),
}

const mapActionCreators = {
  logIn,
}

const mapStateToProps = (state) => ({
  loggedIn: state.user.loggedIn,
  isFetching: state.user.isFetching,
})

export default connect(mapStateToProps, mapActionCreators)(reduxForm(form)(Login))

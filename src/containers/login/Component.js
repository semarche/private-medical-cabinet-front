import React from 'react'
import PropTypes from 'prop-types'
import { Redirect } from 'react-router-dom'
import { Field } from 'redux-form'
import { Form, Icon, Input, Button, Checkbox } from 'antd'
import './index.css'

const FormItem = Form.Item

const propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  loggedIn: PropTypes.bool.isRequired,
  isFetching: PropTypes.bool.isRequired,
}
const defaultProps = {
  loggedIn: false,
  isFetching: false,
}

const Email = ({ input }) =>
  <Input
    prefix={<Icon type='user' />}
    placeholder='Username'
    value={input.value}
    onChange={e => input.onChange(e.target.value)}
  />

const Password = ({ input }) =>
  <Input
    prefix={<Icon type='lock' />}
    placeholder='Password'
    type='password'
    value={input.value}
    onChange={e => input.onChange(e.target.value)}
  />

const Login = ({ handleSubmit, loggedIn, isFetching }) => (loggedIn ? <Redirect to={'/'} /> : (
  <div className='login-page'>
    <div className='login-form'>
      <Form onSubmit={handleSubmit}>
        <FormItem>
          <Field
            name='email'
            component={Email}
          />
        </FormItem>
        <FormItem>
          <Field
            name='password'
            component={Password}
          />
        </FormItem>
        <FormItem>
          <Checkbox>Remember me</Checkbox>
          {/* <a className='login-form-forgot' href=''>Forgot password</a> */}
          <Button type='primary' htmlType='submit' className='login-form-button' loading={isFetching}>
            Log in
          </Button>
          {/* Or <a href=''>register now!</a> */}
        </FormItem>
      </Form>
    </div>
  </div>
))

Login.propTypes = propTypes
Login.defaultProps = defaultProps

export default Login

import React from 'react'
import { Route, Switch } from 'react-router-dom'

import List from './list'
import Manipulation from './manipulation'

const Manipulations = () => (
  <Switch>
    <Route exact path='/manipulations' component={List} />
    <Route path='/manipulations/:manipulation_id' component={Manipulation} />
  </Switch>
)

export default Manipulations

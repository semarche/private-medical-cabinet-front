import { connect } from 'react-redux'

import { getManipulationList } from '../../../modules/manipulations'

import Component from './Manipulations'

const mapActionCreators = {
  getManipulationList,
}

const mapStateToProps = (state) => ({
  manipulationList: state.manipulations.manipulationList,
  count: 200,
  isFetching: state.manipulations.isFetching,
})

export default connect(mapStateToProps, mapActionCreators)(Component)

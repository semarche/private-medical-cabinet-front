/*  eslint no-underscore-dangle: ["error", { "allow": ["_id"] }]*/

import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import moment from 'moment'
import PropTypes from 'prop-types'
import { Table, Input, Button } from 'antd'
import queryString from 'query-string'
import { history } from '../../../store'

import './index.css'

const Search = Input.Search

const propTypes = {
  manipulationList: PropTypes.array.isRequired,
  getManipulationList: PropTypes.func.isRequired,
  isFetching: PropTypes.bool.isRequired,
  location: PropTypes.object.isRequired,
}
const defaultProps = {
  manipulationList: [],
  isFetching: false,
}

const columns = [
  {
    title: 'Client',
    dataIndex: 'clientFullName',
    sorter: true,
    render: (name, record) => <Link to={`/clients/${record.clientId}`}>{name}</Link>,
  },
  {
    title: 'Date',
    dataIndex: 'date',
    sorter: true,
    render: date => moment(date).format('DD/MM/YY'),
  },
  {
    title: 'Manipulation',
    dataIndex: 'manipulationName',
    render: (manipulation, record) => <Link to={`/manipulations/${record._id}`}>{manipulation}</Link>,
  },
  {
    title: 'Cost',
    dataIndex: 'cost',
  },
  {
    title: 'Desctiption',
    dataIndex: 'description',
  },
]

export class Manipulations extends Component {
  constructor() {
    super()
    this.state = {
      pagination: {},
    }
    this.clearSearch = this.clearSearch.bind(this)
  }

  componentWillMount() {
    this.parsed = queryString.parse(this.props.location.search)
  }

  componentDidMount() {
    const { getManipulationList } = this.props
    getManipulationList()
  }

  handleTableChange = (pagination, filters, sorter) => {
    console.log(pagination, filters, sorter)
  }

  clearSearch() {
    this.searchField.input.refs.input.value = ''
    history.push('/manipulations')
    this.props.getManipulationList()
  }

  render() {
    const { manipulationList, isFetching, getManipulationList } = this.props
    return (
      <div className='manipulations'>
        <div className='first-row'>
          <Search
            ref={node => (this.searchField = node)}
            placeholder='Type to search by client'
            defaultValue={this.parsed.search || ''}
            size={'large'}
            onSearch={value => {
              history.push(`/manipulations${value ? `?search=${value}` : ''}`)
              getManipulationList()
            }}
          />
          <Button
            size='large'
            icon='reload'
            onClick={this.clearSearch}
          />
        </div>
        <Table
          columns={columns}
          rowKey={record => record._id}
          dataSource={manipulationList}
          // pagination={this.state.pagination}
          pagination={false}
          loading={isFetching}
          onChange={this.handleTableChange}
        />
      </div>
    )
  }
}


Manipulations.propTypes = propTypes
Manipulations.defaultProps = defaultProps
export default Manipulations

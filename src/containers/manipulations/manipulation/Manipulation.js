import React, { Component } from 'react'
import { Form, Input, DatePicker, Modal, Button, InputNumber } from 'antd'
import PropTypes from 'prop-types'
import { Field } from 'redux-form'
import queryString from 'query-string'
import { history } from '../../../store'
import './index.css'

const FormItem = Form.Item;
// const Option = Select.Option;
const { TextArea } = Input;

const propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  getManipulation: PropTypes.func.isRequired,
  removeManipulation: PropTypes.func.isRequired,
  clearManipulaionState: PropTypes.func.isRequired,
  manipulationId: PropTypes.string.isRequired,
  location: PropTypes.object.isRequired,
}

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 6 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 14,
      offset: 6,
    },
  },
}

const ManipulationName = ({ input }) =>
  <Input
    placeholder='Manipulation'
    value={input.value}
    onChange={e => input.onChange(e.target.value)}
  />
const Description = ({ input }) =>
  <TextArea
    placeholder='Description'
    autosize={{ minRows: 1, maxRows: 99 }}
    value={input.value}
    onChange={e => input.onChange(e.target.value)}
  />
const Cost = ({ input }) =>
  <InputNumber
    min={0}
    formatter={value => `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
    parser={value => value.replace(/\$\s?|(,*)/g, '')}
    value={input.value}
    onChange={input.onChange}
  />
const Date = ({ input }) =>
  <DatePicker
    format='DD/MM/YYYY'
    placeholder='Please select date!'
    value={input.value}
    onChange={input.onChange}
  />
const confirm = (func) => {
  Modal.confirm({
    title: 'Confirm',
    content: 'Are you sure you want to delete manipulation?',
    okText: 'Yes',
    okType: 'danger',
    cancelText: 'No',
    onOk() {
      func().then(() => history.push('/manipulations'))
    },
  });
}

export class Manipulation extends Component {

  componentDidMount() {
    const { manipulationId, getManipulation, location } = this.props
    if (manipulationId !== 'new') {
      return getManipulation(manipulationId)
    }
    const parsed = queryString.parse(location.search)
    if (!parsed.clientFullName || !parsed.clientId) {
      return history.push('/clients')
    }
    return null
  }

  componentWillUnmount() {
    this.props.clearManipulaionState()
  }

  render() {
    const { manipulationId, removeManipulation, handleSubmit } = this.props
    return (
      <div className='client'>
        <div className={'title'}>
          {manipulationId === 'new' ?
            <h2>Add new manipulation</h2> : null
          }
        </div>
        <Form onSubmit={handleSubmit}>
          <FormItem
            {...formItemLayout}
            label='Date'
          >
            <Field
              name='date'
              component={Date}
            />
          </FormItem>
          <FormItem
            {...formItemLayout}
            label='Manipulation'
          >
            <Field
              name='manipulationName'
              component={ManipulationName}
            />
          </FormItem>
          <FormItem
            {...formItemLayout}
            label='Description'
          >
            <Field
              name='description'
              component={Description}
            />
          </FormItem>
          <FormItem
            {...formItemLayout}
            label='Cost'
          >
            <Field
              name='cost'
              component={Cost}
            />
          </FormItem>

          <FormItem {...tailFormItemLayout} className={'form-buttons'}>
            <Button onClick={() => history.push('/clients')}>Cancel</Button>
            {' '}
            {manipulationId !== 'new' ?
              <Button type='danger' onClick={() => confirm(() => removeManipulation(manipulationId))}>
                Delete
              </Button> : null
            }
            {' '}
            <Button type='primary' htmlType='submit'>{manipulationId === 'new' ? 'Create' : 'Save'}</Button>
          </FormItem>
        </Form>
      </div>
    );
  }
}

Manipulation.propTypes = propTypes
export default Manipulation

// <Input addonBefore={prefixSelector} style={{ width: '100%' }} />
//   <Select>
//     <Option value='099'>+38099</Option>
//     <Option value='098'>+38098</Option>
//     <Option value='097'>+38097</Option>
//     <Option value='096'>+38096</Option>
//     <Option value='095'>+38095</Option>
//     <Option value='093'>+38093</Option>
//     <Option value='068'>+38068</Option>
//     <Option value='067'>+38067</Option>
//     <Option value='066'>+38066</Option>
//     <Option value='063'>+38063</Option>
//     <Option value='050'>+38050</Option>
//   </Select>
// )


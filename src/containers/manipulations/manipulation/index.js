import { connect } from 'react-redux'
import moment from 'moment'
import { reduxForm } from 'redux-form'
import { history } from '../../../store'
import queryString from 'query-string'
import { obj } from '../../../modules/utils'

import Component from './Manipulation'
import { getManipulation, addManipulation, removeManipulation, replaceManipulation,
  clearManipulaionState } from '../../../modules/manipulations'

const form = {
  form: 'manipulation-form',
  enableReinitialize: true,
  validate: (values, props) => {
    const errors = {}
    if (!props.dirty) {
      return errors
    }
    if (!values.manipulationName) {
      errors.manipulationName = 'Manipulation is empty'
    }
    return errors
  },
  onSubmit: (values, dispatch, props) => {
    const { manipulationId, location, clientFullName, clientId } = props
    if (manipulationId === 'new') {
      const parsed = queryString.parse(location.search)
      return props.addManipulation({
        ...values,
        date: values.date.format(),
        clientFullName: parsed.clientFullName,
        clientId: parsed.clientId,
      }).then(() => history.push('/manipulations'))
    }
    return props.replaceManipulation(manipulationId, {
      ...values,
      clientFullName,
      clientId,
      date: values.date.format(),
    }).then(() => history.push('/manipulations'))
  },
}

const mapActionCreators = {
  getManipulation,
  addManipulation,
  removeManipulation,
  replaceManipulation,
  clearManipulaionState,
}

const mapStateToProps = (state, ownProps) => ({
  manipulationId: obj.deepGet(ownProps, ['match', 'params', 'manipulation_id']),
  clientFullName: state.manipulations.manipulation.clientFullName,
  clientId: state.manipulations.manipulation.clientId,
  initialValues: {
    date: moment(state.manipulations.manipulation.date),
    manipulationName: state.manipulations.manipulation.manipulationName,
    description: state.manipulations.manipulation.description,
    cost: state.manipulations.manipulation.cost,
  },
})

export default connect(mapStateToProps, mapActionCreators)(reduxForm(form)(Component))

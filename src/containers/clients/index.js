import React from 'react'
import { Route, Switch } from 'react-router-dom'

import List from './list'
import Client from './client'

const Clients = () => (
  <Switch>
    <Route exact path='/clients' component={List} />
    <Route path='/clients/:client_id' component={Client} />
  </Switch>
)

export default Clients

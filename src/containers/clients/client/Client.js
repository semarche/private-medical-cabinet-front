import React, { Component } from 'react'
import { Form, Input, Modal, Icon, Rate, Button } from 'antd'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import { Field, FieldArray } from 'redux-form'
import queryString from 'query-string'
import { history } from '../../../store'
import './index.css'

const FormItem = Form.Item
// const Option = Select.Option;
const { TextArea } = Input

const propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  getClient: PropTypes.func.isRequired,
  removeClient: PropTypes.func.isRequired,
  clearClientState: PropTypes.func.isRequired,
  clientId: PropTypes.string.isRequired,
  fullName: PropTypes.string.isRequired,
}

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 6 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
}
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 14,
      offset: 6,
    },
  },
}
const Phone = ({ input }) =>
  <Input
    placeholder='Phone number'
    {...input}
  />
const renderTelephones = ({ fields, meta: { error } }) => (
  <div>
    {fields.map((telephone, index) =>
      <FormItem
        className={'inline-fields telephone'}
        {...formItemLayout}
        key={index}
      >
        <Field
          name={`${telephone}.number`}
          component={Phone}
        />
        <Button
          className={'remove-phone-button'}
          type='danger'
          shape='circle'
          icon='delete'
          onClick={() => fields.remove(index)}
        />
      </FormItem>
    )}
    <Button className={'add-phone-button'} type='dashed' onClick={() => fields.push()}>
      <Icon type='plus' /> Add phone
    </Button>
  </div>
)
const Illness = ({ input }) =>
  <Input
    placeholder='Illness'
    value={input.value}
    onChange={e => input.onChange(e.target.value)}
  />
const From = ({ input }) =>
  <Input
    placeholder='From'
    value={input.value}
    onChange={e => input.onChange(e.target.value)}
  />
const ClientFullName = ({ input }) =>
  <Input
    placeholder='Client full name'
    value={input.value}
    onChange={e => input.onChange(e.target.value)}
  />
const Profession = ({ input }) =>
  <Input
    placeholder='Profession'
    value={input.value}
    onChange={e => input.onChange(e.target.value)}
  />
const Description = ({ input }) =>
  <TextArea
    placeholder='Description'
    autosize={{ minRows: 1, maxRows: 99 }}
    value={input.value}
    onChange={e => input.onChange(e.target.value)}
  />
const Age = ({ input }) =>
  <Input
    placeholder='Age'
    value={input.value}
    onChange={e => input.onChange(e.target.value)}
  />
const Rating = ({ input }) =>
  <Rate
    value={input.value}
    onChange={e => input.onChange(e)}
  />
const confirm = (func) => {
  Modal.confirm({
    title: 'Confirm',
    content: 'Are you sure you want to delete client?',
    okText: 'Yes',
    okType: 'danger',
    cancelText: 'No',
    onOk() {
      func().then(() => history.push('/clients'))
    },
  });
}

export class Client extends Component {
  constructor() {
    super()
    this.addManipulation = this.addManipulation.bind(this)
  }

  componentDidMount() {
    const { clientId, getClient } = this.props
    if (clientId !== 'new') {
      getClient(clientId)
    }
  }

  componentWillUnmount() {
    this.props.clearClientState()
  }

  addManipulation() {
    const { clientId, fullName } = this.props
    const search = queryString.stringify({
      clientId,
      clientFullName: fullName,
    })
    history.push({
      pathname: '/manipulations/new',
      search,
    })
  }

  render() {
    const { clientId, removeClient, fullName } = this.props
    return (
      <div className='client'>
        <div className={'title'}>
          {clientId === 'new' ?
            <h2>New client card</h2> :
            <div>
              <Link to={`/manipulations?clientId=${clientId}&search=${fullName}`}>
                <Button
                  icon='medicine-box'
                >
                  Show manipulations
                </Button>
              </Link>
              {' '}
              <Button
                type='primary'
                icon='plus-circle-o'
                onClick={this.addManipulation}
              >
                Add new manipulation
              </Button>
            </div>
          }
        </div>
        <Form onSubmit={this.props.handleSubmit}>
          <FormItem
            {...formItemLayout}
            label='Illness'
          >
            <Field
              name='illness'
              component={Illness}
            />
          </FormItem>
          <FormItem
            {...formItemLayout}
            label='From'
          >
            <Field
              name='from'
              component={From}
            />
          </FormItem>
          <FormItem
            {...formItemLayout}
            label='Full name'
          >
            <Field
              name='fullName'
              component={ClientFullName}
            />
          </FormItem>
          <FormItem
            {...formItemLayout}
            label='Profession'
          >
            <Field
              name='profession'
              component={Profession}
            />
          </FormItem>
          <FormItem
            {...formItemLayout}
            label='Phone number'
          >
            <FieldArray
              name='telephoneNumber'
              component={renderTelephones}
            />
          </FormItem>
          <FormItem
            {...formItemLayout}
            label='Description'
          >
            <Field
              name='description'
              component={Description}
            />
          </FormItem>
          <FormItem
            {...formItemLayout}
            label='Age'
          >
            <Field
              name='age'
              component={Age}
            />
          </FormItem>
          <FormItem
            {...formItemLayout}
            label='Rating'
          >
            <Field
              name='rating'
              component={Rating}
            />
          </FormItem>
          <FormItem {...tailFormItemLayout} className={'form-buttons'}>
            <Button onClick={() => history.push('/clients')}>Cancel</Button>
            {' '}
            {clientId !== 'new' ?
              <Button type='danger' onClick={() => confirm(() => removeClient(clientId))}>Delete</Button> : null
            }
            {' '}
            <Button type='primary' htmlType='submit'>{clientId === 'new' ? 'Create' : 'Save'}</Button>
          </FormItem>
        </Form>
      </div>
    );
  }
}

Client.propTypes = propTypes
export default Client

// <Input addonBefore={prefixSelector} style={{ width: '100%' }} />
//   <Select>
//     <Option value='099'>+38099</Option>
//     <Option value='098'>+38098</Option>
//     <Option value='097'>+38097</Option>
//     <Option value='096'>+38096</Option>
//     <Option value='095'>+38095</Option>
//     <Option value='093'>+38093</Option>
//     <Option value='068'>+38068</Option>
//     <Option value='067'>+38067</Option>
//     <Option value='066'>+38066</Option>
//     <Option value='063'>+38063</Option>
//     <Option value='050'>+38050</Option>
//   </Select>
// )


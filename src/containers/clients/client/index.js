import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import { history } from '../../../store'
import { obj } from '../../../modules/utils'

import Component from './Client'
import { addClient, getClient, removeClient, clearClientState, replaceClient } from '../../../modules/clients'

const form = {
  form: 'client-form',
  enableReinitialize: true,
  validate: (values, props) => {
    const errors = {}
    if (!props.dirty) {
      return errors
    }
    if (!values.fullName) {
      errors.fullName = 'Full name is empty'
    }
    return errors
  },
  onSubmit: (values, dispatch, props) => {
    const { clientId } = props
    if (clientId === 'new') {
      return props.addClient({
        ...values,
        telephoneNumber: values.telephoneNumber.filter(t => t.number).map(t => t.number),
      }).then(() => history.push('/clients'))
    }
    return props.replaceClient(clientId, {
      ...values,
      telephoneNumber: values.telephoneNumber.filter(t => t.number).map(t => t.number),
    }).then(() => history.push('/clients'))
  },
}

const mapActionCreators = {
  addClient,
  getClient,
  removeClient,
  clearClientState,
  replaceClient,
}

const mapStateToProps = (state, ownProps) => ({
  clientId: obj.deepGet(ownProps, ['match', 'params', 'client_id']),
  fullName: state.clients.client.fullName,
  initialValues: {
    illness: state.clients.client.illness,
    from: state.clients.client.from,
    fullName: state.clients.client.fullName,
    profession: state.clients.client.profession,
    rating: state.clients.client.rating,
    telephoneNumber: state.clients.client.telephoneNumber.map(t => ({ number: t })),
    age: state.clients.client.age,
    description: state.clients.client.description,
  },
})

export default connect(mapStateToProps, mapActionCreators)(reduxForm(form)(Component))

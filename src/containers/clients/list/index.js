import { connect } from 'react-redux'

import { getClientList } from '../../../modules/clients'

import Component from './Clients'

const mapActionCreators = {
  getClientList,
}

const mapStateToProps = (state) => ({
  clientList: state.clients.clientList,
  count: 200,
  isFetching: state.clients.isFetching,
})

export default connect(mapStateToProps, mapActionCreators)(Component)

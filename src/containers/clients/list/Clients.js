/*  eslint no-underscore-dangle: ["error", { "allow": ["_id"] }]*/

import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import { Table, Input, Button } from 'antd'
import { history } from '../../../store'

import './index.css'

const Search = Input.Search

const propTypes = {
  clientList: PropTypes.array.isRequired,
  getClientList: PropTypes.func.isRequired,
  isFetching: PropTypes.bool.isRequired,
}
const defaultProps = {
  clientList: [],
  isFetching: false,
}

const columns = [
  {
    title: 'Ilness',
    dataIndex: 'illness',
    sorter: true,
  },
  {
    title: 'From',
    dataIndex: 'from',
  },
  {
    title: 'Full name',
    dataIndex: 'fullName',
    render: (name, record) => <Link to={`/clients/${record._id}`}>{name}</Link>,
  },
  {
    title: 'Profession',
    dataIndex: 'profession',
  },
  {
    title: 'Age',
    dataIndex: 'age',
  },
  {
    title: 'Rating',
    dataIndex: 'rating',
  },
  {
    title: 'Telephone number',
    dataIndex: 'telephoneNumber',
    render: tel => (tel.length ? tel.map((t, i) => <p key={i}>{t}</p>) : ''),
  },
  {
    title: 'Desctiption',
    dataIndex: 'description',
  },
  {
    render: (name, record) => <Link to={`/manipulations?clientId=${record._id}&search=${record.fullName}`}>
      <Button icon='medicine-box' />
    </Link>,
  },
]

export class Clients extends Component {
  constructor() {
    super()
    this.state = {
      pagination: {},
    }
    this.clearSearch = this.clearSearch.bind(this)
  }
  componentDidMount() {
    this.props.getClientList()
  }
  handleTableChange = (pagination, filters, sorter) => {
    console.log(pagination, filters, sorter)
  }
  clearSearch() {
    this.searchField.input.refs.input.value = ''
    history.push('/clients')
    this.props.getClientList()
  }
  render() {
    const { clientList, isFetching, getClientList } = this.props
    return (
      <div className='clients'>
        <div className='first-row'>
          <Link to={'/clients/new'}>
            <Button size='large' type='primary' icon='user-add'>Add new</Button>
          </Link>
          <Search
            ref={node => (this.searchField = node)}
            placeholder='Type to search clients...'
            size={'large'}
            onSearch={value => {
              history.push(`/clients?search=${value ? `?search=${value}` : ''}`)
              getClientList()
            }}
          />
          <Button
            size='large'
            icon='reload'
            onClick={this.clearSearch}
          />
        </div>
        <Table
          columns={columns}
          rowKey={record => record._id}
          dataSource={clientList}
          // pagination={this.state.pagination}
          pagination={false}
          loading={isFetching}
          onChange={this.handleTableChange}
        />
      </div>
    )
  }
}


Clients.propTypes = propTypes
Clients.defaultProps = defaultProps
export default Clients

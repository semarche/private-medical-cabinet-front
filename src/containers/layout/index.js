import { connect } from 'react-redux'
import { logOut } from '../../modules/user'
import Component from './MainLayout'

const mapActionCreators = {
  logOut,
}

const mapStateToProps = (state) => ({
  loggedIn: state.user.loggedIn,
})

export default connect(mapStateToProps, mapActionCreators)(Component)

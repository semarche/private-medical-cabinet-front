import React, { Component } from 'react'
import { Route, Redirect, Switch } from 'react-router-dom'
import PropTypes from 'prop-types'
import { Layout } from 'antd'
import Header from './header'
import Clients from '../clients'
import Manipulations from '../manipulations'
import Schedule from '../schedule'
import Statistics from '../statistics'
import NotFound from '../404'

import './index.css'

const { Content } = Layout

const propTypes = {
  loggedIn: PropTypes.bool.isRequired,
}
const defaultProps = {
  loggedIn: false,
  user: null,
  token: null,
}

export class MainLayout extends Component {
  render() {
    const { loggedIn } = this.props
    return (
      !loggedIn ? <Redirect to={'/login'} /> : (
        <Layout>
          <Header />
          <Content>
            <Switch>
              <Redirect from='/' exact to='/clients' />
              <Route path='/clients' component={Clients} />
              <Route path='/manipulations' component={Manipulations} />
              <Route exact path='/schedule' component={Schedule} />
              <Route exact path='/statistics' component={Statistics} />
              <Route component={NotFound} />
            </Switch>
          </Content>
        </Layout>
      )
    )
  }
}

MainLayout.propTypes = propTypes
MainLayout.defaultProps = defaultProps
export default MainLayout

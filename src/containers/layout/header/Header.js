import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import { Layout, Menu, Icon } from 'antd'
import './index.css'

const { Header } = Layout

const propTypes = {
  logOut: PropTypes.func.isRequired,
  location: PropTypes.object.isRequired,
}

const getActive = (param) => {
  if (param.includes('schedule')) return '3'
  if (param.includes('manipulations')) return '2'
  if (param.includes('statistics')) return '4'
  return '1'
}

const HeaderLayout = ({ logOut, location }) => (
  <Header className='header'>
    <div className={'menu-container'}>
      <Menu
        theme='dark'
        mode='horizontal'
        selectedKeys={[getActive(location.pathname)]}
      >
        <Menu.Item key='1'>
          <Link to='/clients'><Icon type='team' /><span className={'menu-title'}>Clients</span></Link>
        </Menu.Item>
        <Menu.Item key='2'>
          <Link to='/manipulations'><Icon type='medicine-box' /><span className={'menu-title'}>Manipulations</span></Link>
        </Menu.Item>
        <Menu.Item key='3'>
          <Link to='/schedule'><Icon type='schedule' /><span className={'menu-title'}>Calendar</span></Link>
        </Menu.Item>
        <Menu.Item key='4'>
          <Link to='/statistics'><Icon type='area-chart' /><span className={'menu-title'}>Statistics</span></Link>
        </Menu.Item>
      </Menu>
    </div>
    <div className={'logOut'}>
      <Icon type='logout' onClick={logOut} />
    </div>
  </Header>
)

HeaderLayout.propTypes = propTypes
export default HeaderLayout

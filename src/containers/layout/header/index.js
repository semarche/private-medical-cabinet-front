import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { logOut } from '../../../modules/user'
import Component from './Header'

const mapActionCreators = {
  logOut,
}

// const mapStateToProps = () => ({
//
// })

export default withRouter(connect(null, mapActionCreators)(Component))

import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { LocaleProvider } from 'antd'
import enUS from 'antd/lib/locale-provider/en_US'

import Layout from '../layout'
import Login from '../login'

const App = () => (
  <LocaleProvider locale={enUS}>
    <Switch>
      <Route exact path='/login' component={Login} />
      <Route path='/' component={Layout} />
    </Switch>
  </LocaleProvider>
)

export default App

import { connect } from 'react-redux'

import Component from './Statistics'

const mapActionCreators = {
}

const mapStateToProps = (state) => ({

})

export default connect(mapStateToProps, mapActionCreators)(Component)

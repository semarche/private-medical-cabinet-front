import { Record as record } from 'immutable'
import { history } from '../store'
import { obj, HTTP } from './utils'

export const consts = {
  MANIPULATION_LIST_REQUEST: 'manipulations/MANIPULATION_LIST_REQUEST',
  MANIPULATION_LIST_SUCCESS: 'manipulations/MANIPULATION_LIST_SUCCESS',
  MANIPULATION_LIST_FAIL: 'manipulations/MANIPULATION_LIST_FAIL',

  GET_MANIPULATION_REQUEST: 'manipulations/GET_MANIPULATION_REQUEST',
  GET_MANIPULATION_SUCCESS: 'manipulations/GET_MANIPULATION_SUCCESS',
  GET_MANIPULATION_FAIL: 'manipulations/GET_MANIPULATION_FAIL',

  ADD_MANIPULATION_REQUEST: 'manipulations/ADD_MANIPULATION_REQUEST',
  ADD_MANIPULATION_SUCCESS: 'manipulations/ADD_MANIPULATION_SUCCESS',
  ADD_MANIPULATION_FAIL: 'manipulations/ADD_MANIPULATION_FAIL',

  REMOVE_MANIPULATION_REQUEST: 'manipulations/REMOVE_MANIPULATION_REQUEST',
  REMOVE_MANIPULATION_SUCCESS: 'manipulations/REMOVE_MANIPULATION_SUCCESS',
  REMOVE_MANIPULATION_FAIL: 'manipulations/REMOVE_MANIPULATION_FAIL',

  REPLACE_MANIPULATION_REQUEST: 'manipulations/REPLACE_MANIPULATION_REQUEST',
  REPLACE_MANIPULATION_SUCCESS: 'manipulations/REPLACE_MANIPULATION_SUCCESS',
  REPLACE_MANIPULATION_FAIL: 'manipulations/REPLACE_MANIPULATION_FAIL',

  CLEAR_MANIPULATION_STATE: 'manipulations/CLEAR_MANIPULATION_STATE',
}

const Manipulation = record({
  date: new Date(),
  clientId: '',
  clientFullName: '',
  manipulationName: '',
  description: '',
  cost: 0,
})

const InitialState = record({
  isFetching: false,
  manipulationList: [],
  manipulation: new Manipulation(),
  error: null,
})

export const getManipulationList = () => (dispatch) => {
  dispatch({ type: consts.MANIPULATION_LIST_REQUEST })
  return HTTP.get(`/manipulation${history.location.search}`)
    .then(response => dispatch({ type: consts.MANIPULATION_LIST_SUCCESS, response }))
    .catch(error => dispatch({ type: consts.MANIPULATION_LIST_FAIL, error }))
}

export const getManipulation = (id) => (dispatch) => {
  dispatch({ type: consts.GET_MANIPULATION_REQUEST })
  return HTTP.get(`/manipulation/${id}`)
    .then(response => dispatch({ type: consts.GET_MANIPULATION_SUCCESS, response }))
    .catch(error => dispatch({ type: consts.GET_MANIPULATION_FAIL, error }))
}

export const addManipulation = (data) => (dispatch) => {
  dispatch({ type: consts.ADD_MANIPULATION_REQUEST })
  return HTTP.post('/manipulation', data)
    .then(response => dispatch({ type: consts.ADD_MANIPULATION_SUCCESS, response }))
    .catch(error => dispatch({ type: consts.ADD_MANIPULATION_FAIL, error }))
}

export const removeManipulation = (id) => (dispatch) => {
  dispatch({ type: consts.REMOVE_MANIPULATION_REQUEST })
  return HTTP.delete(`/manipulation/${id}`)
    .then(response => dispatch({ type: consts.REMOVE_MANIPULATION_SUCCESS, response }))
    .catch(error => dispatch({ type: consts.REMOVE_MANIPULATION_FAIL, error }))
}

export const replaceManipulation = (id, data) => (dispatch) => {
  dispatch({ type: consts.REPLACE_MANIPULATION_REQUEST })
  return HTTP.put(`/manipulation/${id}`, data)
    .then(response => dispatch({ type: consts.REPLACE_MANIPULATION_SUCCESS, response }))
    .catch(error => dispatch({ type: consts.REPLACE_MANIPULATION_FAIL, error }))
}

export const clearManipulaionState = () => (dispatch) => dispatch({ type: consts.CLEAR_MANIPULATION_STATE })


export const actionHandlers = {
  [consts.MANIPULATION_LIST_REQUEST]: (state) => state.withMutations((ctx) =>
    ctx.set('isFetching', true).set('error', null)),
  [consts.MANIPULATION_LIST_SUCCESS]: (state, action) => state.withMutations((ctx) => {
    const data = obj.deepGet(action, ['response', 'data'], [])
    ctx.set('isFetching', false)
      .set('error', null)
      .set('manipulationList', data)
  }),
  [consts.MANIPULATION_LIST_FAIL]: (state, action) => state.withMutations((ctx) =>
    ctx.set('isFetching', false).set('error', action.error)),

  [consts.GET_MANIPULATION_REQUEST]: (state) => state.withMutations((ctx) =>
    ctx.set('isFetching', true).set('error', null)),
  [consts.GET_MANIPULATION_SUCCESS]: (state, action) => state.withMutations((ctx) => {
    const data = obj.deepGet(action, ['response', 'data'], {})
    ctx.set('isFetching', false)
      .set('error', null)
      .set('manipulation', new Manipulation(data))
  }),
  [consts.GET_MANIPULATION_FAIL]: (state, action) => state.withMutations((ctx) =>
    ctx.set('isFetching', false).set('error', action.error)),

  [consts.ADD_MANIPULATION_REQUEST]: (state) => state.withMutations((ctx) =>
    ctx.set('isFetching', true).set('error', null)),
  [consts.ADD_MANIPULATION_SUCCESS]: (state) => state.withMutations((ctx) => ctx.set('isFetching', false)),
  [consts.ADD_MANIPULATION_FAIL]: (state, action) => state.withMutations((ctx) =>
    ctx.set('isFetching', false).set('error', action.error)),

  [consts.REMOVE_MANIPULATION_REQUEST]: (state) => state.withMutations((ctx) =>
    ctx.set('isFetching', true).set('error', null)),
  [consts.REMOVE_MANIPULATION_SUCCESS]: (state) => state.withMutations((ctx) => ctx.set('isFetching', false)),
  [consts.REMOVE_MANIPULATION_FAIL]: (state, action) => state.withMutations((ctx) =>
    ctx.set('isFetching', false).set('error', action.error)),

  [consts.REPLACE_MANIPULATION_REQUEST]: (state) => state.withMutations((ctx) =>
    ctx.set('isFetching', true).set('error', null)),
  [consts.REPLACE_MANIPULATION_SUCCESS]: (state) => state.withMutations((ctx) => ctx.set('isFetching', false)),
  [consts.REPLACE_MANIPULATION_FAIL]: (state, action) => state.withMutations((ctx) =>
    ctx.set('isFetching', false).set('error', action.error)),

  [consts.CLEAR_MANIPULATION_STATE]: (state) => state.withMutations((ctx) =>
    ctx.set('manipulation', new Manipulation())),
}

const initialState = new InitialState()
export const reducers = (state = initialState, action) => {
  const handler = actionHandlers[action.type]
  return handler ? handler(state, action) : state
}

export default reducers

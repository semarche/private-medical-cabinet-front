import { Record as record } from 'immutable'
import axios from 'axios'
import { apiPath } from './const'
import { obj } from './utils'

export const consts = {
  USER_LIST_REQUEST: 'admin/USER_LIST_REQUEST',
  USER_LIST_SUCCESS: 'admin/LUSER_LIST_SUCCESS',
  USER_LIST_FAIL: 'admin/USER_LIST_FAIL',
}

const InitialState = record({
  isFetching: false,
  userList: [],
})

export const getUserList = () => (dispatch) => {
  dispatch({ type: consts.USER_LIST_REQUEST })
  axios.get(`${apiPath}/v1/users`)
    .then(response => dispatch({ type: consts.USER_LIST_SUCCESS, response }))
    .catch(error => dispatch({ type: consts.USER_LIST_FAIL, error }))
}

export const actionHandlers = {
  [consts.USER_LIST_REQUEST]: (state) => state.withMutations((ctx) => ctx.set('isFetching', true)),
  [consts.USER_LIST_SUCCESS]: (state, action) => state.withMutations((ctx) => {
    const userList = obj.deepGet(action, ['response', 'data'])
    ctx.set('isFetching', false)
      .set('error', null)
      .set('userList', userList)
  }),
  [consts.USER_LIST_FAIL]: (state, action) => state.withMutations((ctx) =>
    ctx.set('isFetching', false)
      .set('error', action.error)),

}

const initialState = new InitialState()
export const reducers = (state = initialState, action) => {
  const handler = actionHandlers[action.type]
  return handler ? handler(state, action) : state
}

export default reducers

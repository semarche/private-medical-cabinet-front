import { Record as record } from 'immutable'
import { history } from '../store'
import { obj, HTTP } from './utils'

export const consts = {
  CLIENT_LIST_REQUEST: 'clients/CLIENT_LIST_REQUEST',
  CLIENT_LIST_SUCCESS: 'clients/CLIENT_LIST_SUCCESS',
  CLIENT_LIST_FAIL: 'clients/CLIENT_LIST_FAIL',

  GET_CLIENT_REQUEST: 'clients/GET_CLIENT_REQUEST',
  GET_CLIENT_SUCCESS: 'clients/GET_CLIENT_SUCCESS',
  GET_CLIENT_FAIL: 'clients/GET_CLIENT_FAIL',

  ADD_CLIENT_REQUEST: 'clients/ADD_CLIENT_REQUEST',
  ADD_CLIENT_SUCCESS: 'clients/ADD_CLIENT_SUCCESS',
  ADD_CLIENT_FAIL: 'clients/ADD_CLIENT_FAIL',

  REMOVE_CLIENT_REQUEST: 'clients/REMOVE_CLIENT_REQUEST',
  REMOVE_CLIENT_SUCCESS: 'clients/REMOVE_CLIENT_SUCCESS',
  REMOVE_CLIENT_FAIL: 'clients/REMOVE_CLIENT_FAIL',

  REPLACE_CLIENT_REQUEST: 'clients/REPLACE_CLIENT_REQUEST',
  REPLACE_CLIENT_SUCCESS: 'clients/REPLACE_CLIENT_SUCCESS',
  REPLACE_CLIENT_FAIL: 'clients/REPLACE_CLIENT_FAIL',

  CLEAR_CLIENT_STATE: 'clients/CLEAR_CLIENT_STATE',
}

const Client = record({
  illness: '',
  from: '',
  fullName: '',
  profession: '',
  age: '',
  rating: 0,
  telephoneNumber: [''],
  description: '',
})

const InitialState = record({
  isFetching: false,
  clientList: [],
  client: new Client(),
  error: null,
})

export const getClientList = () => (dispatch) => {
  dispatch({ type: consts.CLIENT_LIST_REQUEST })
  return HTTP.get(`/client${history.location.search}`)
    .then(response => dispatch({ type: consts.CLIENT_LIST_SUCCESS, response }))
    .catch(error => dispatch({ type: consts.CLIENT_LIST_FAIL, error }))
}

export const getClient = (id) => (dispatch) => {
  dispatch({ type: consts.GET_CLIENT_REQUEST })
  return HTTP.get(`/client/${id}`)
    .then(response => dispatch({ type: consts.GET_CLIENT_SUCCESS, response }))
    .catch(error => dispatch({ type: consts.GET_CLIENT_FAIL, error }))
}

export const addClient = (client) => (dispatch) => {
  dispatch({ type: consts.ADD_CLIENT_REQUEST })
  return HTTP.post('/client', client)
    .then(response => dispatch({ type: consts.ADD_CLIENT_SUCCESS, response }))
    .catch(error => dispatch({ type: consts.ADD_CLIENT_FAIL, error }))
}

export const removeClient = (id) => (dispatch) => {
  dispatch({ type: consts.REMOVE_CLIENT_REQUEST })
  return HTTP.delete(`/client/${id}`)
    .then(response => dispatch({ type: consts.REMOVE_CLIENT_SUCCESS, response }))
    .catch(error => dispatch({ type: consts.REMOVE_CLIENT_FAIL, error }))
}

export const replaceClient = (id, client) => (dispatch) => {
  dispatch({ type: consts.REPLACE_CLIENT_REQUEST })
  return HTTP.put(`/client/${id}`, client)
    .then(response => dispatch({ type: consts.REPLACE_CLIENT_SUCCESS, response }))
    .catch(error => dispatch({ type: consts.REPLACE_CLIENT_FAIL, error }))
}

export const clearClientState = () => (dispatch) => dispatch({ type: consts.CLEAR_CLIENT_STATE })


export const actionHandlers = {
  [consts.CLIENT_LIST_REQUEST]: (state) => state.withMutations((ctx) => ctx.set('isFetching', true).set('error', null)),
  [consts.CLIENT_LIST_SUCCESS]: (state, action) => state.withMutations((ctx) => {
    const clientList = obj.deepGet(action, ['response', 'data'], [])
    ctx.set('isFetching', false)
      .set('error', null)
      .set('clientList', clientList)
  }),
  [consts.CLIENT_LIST_FAIL]: (state, action) => state.withMutations((ctx) =>
    ctx.set('isFetching', false).set('error', action.error)),

  [consts.GET_CLIENT_REQUEST]: (state) => state.withMutations((ctx) => ctx.set('isFetching', true).set('error', null)),
  [consts.GET_CLIENT_SUCCESS]: (state, action) => state.withMutations((ctx) => {
    const client = obj.deepGet(action, ['response', 'data'], {})
    ctx.set('isFetching', false)
      .set('error', null)
      .set('client', new Client(client))
  }),
  [consts.GET_CLIENT_FAIL]: (state, action) => state.withMutations((ctx) =>
    ctx.set('isFetching', false).set('error', action.error)),

  [consts.ADD_CLIENT_REQUEST]: (state) => state.withMutations((ctx) => ctx.set('isFetching', true).set('error', null)),
  [consts.ADD_CLIENT_SUCCESS]: (state) => state.withMutations((ctx) => ctx.set('isFetching', false)),
  [consts.ADD_CLIENT_FAIL]: (state, action) => state.withMutations((ctx) =>
    ctx.set('isFetching', false).set('error', action.error)),

  [consts.REMOVE_CLIENT_REQUEST]: (state) => state.withMutations((ctx) =>
    ctx.set('isFetching', true).set('error', null)),
  [consts.REMOVE_CLIENT_SUCCESS]: (state) => state.withMutations((ctx) => ctx.set('isFetching', false)),
  [consts.REMOVE_CLIENT_FAIL]: (state, action) => state.withMutations((ctx) =>
    ctx.set('isFetching', false).set('error', action.error)),

  [consts.REPLACE_CLIENT_REQUEST]: (state) => state.withMutations((ctx) =>
    ctx.set('isFetching', true).set('error', null)),
  [consts.REPLACE_CLIENT_SUCCESS]: (state) => state.withMutations((ctx) => ctx.set('isFetching', false)),
  [consts.REPLACE_CLIENT_FAIL]: (state, action) => state.withMutations((ctx) =>
    ctx.set('isFetching', false).set('error', action.error)),

  [consts.CLEAR_CLIENT_STATE]: (state) => state.withMutations((ctx) => ctx.set('client', new Client())),
}

const initialState = new InitialState()
export const reducers = (state = initialState, action) => {
  const handler = actionHandlers[action.type]
  return handler ? handler(state, action) : state
}

export default reducers

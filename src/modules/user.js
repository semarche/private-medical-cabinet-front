import { Record as record } from 'immutable'
import ls from 'local-storage'
import { obj, HTTP } from './utils'

const lsUser = ls.get('user')
const lsToken = ls.get('token')
const loggedIn = !!(lsUser && lsToken)

export const consts = {
  LOGIN_REQUEST: 'login/LOGIN_REQUEST',
  LOGIN_SUCCESS: 'login/LOGIN_SUCCESS',
  LOGIN_FAIL: 'login/LOGIN_FAIL',
  LOGOUT: 'login/LOGOUT',
}

const Token = record({
  tokenType: '',
  accessToken: '',
  refreshToken: '',
  expiresIn: '',
})
const User = record({
  id: '',
  email: '',
  role: '',
  createdAt: '',
})

if (loggedIn) {
  console.log(lsToken.accessToken)
}

const InitialState = record({
  isFetching: false,
  loggedIn,
  error: null,
  user: loggedIn ? new User(lsUser) : new User(),
  token: loggedIn ? new Token(lsToken) : new Token(),
})

export const logIn = (email, password) => (dispatch) => {
  dispatch({ type: consts.LOGIN_REQUEST })
  HTTP.post('/auth/login', {
    email,
    password,
  })
    .then(response => {
      const token = obj.deepGet(response, ['data', 'token'], {})
      const user = obj.deepGet(response, ['data', 'user'], {})
      dispatch({ type: consts.LOGIN_SUCCESS, user, token })
    })
    .catch(error => dispatch({ type: consts.LOGIN_FAIL, error }))
}
export const logOut = () => dispatch => dispatch({ type: consts.LOGOUT })

export const actionHandlers = {
  [consts.LOGIN_REQUEST]: (state) => state.withMutations((ctx) => ctx.set('isFetching', true).set('error', null)),
  [consts.LOGIN_SUCCESS]: (state, action) => state.withMutations((ctx) => {
    if (action.user) {
      const user = new User(action.user)
      ls.set('user', user)
      ctx.set('isFetching', false)
        .set('error', null)
        .set('loggedIn', true)
        .set('user', new User(user))
    }
    const token = new Token(action.token)
    ls.set('token', token)
    ctx.set('token', new Token(token))
  }),
  [consts.LOGIN_FAIL]: (state, action) => state.withMutations((ctx) =>
    ctx.set('isFetching', false)
      .set('error', action.error)),

  [consts.LOGOUT]: (state) => state.withMutations((ctx) => {
    ls.remove('user')
    ls.remove('token')
    ctx.set('loggedIn', false)
      .set('user', new User())
      .set('token', new Token())
  }),

}

const initialState = new InitialState()
export const reducers = (state = initialState, action) => {
  const handler = actionHandlers[action.type]
  return handler ? handler(state, action) : state
}

export default reducers

import axios from 'axios'
import { apiPath } from './const'
import store from '../store'
import { consts } from './user'

// console.log('apiPath', apiPath)

const addAuth = (config, user) => {
  const updatedConfig = Object.assign(config, {
    headers: {
      Authorization: `${user.token.tokenType} ${user.token.accessToken}`,
    },
  })
  return updatedConfig
}

const errorHandle = (error) => error.response.status === 401 ?
  store.dispatch({ type: consts.LOGOUT }) :
  Promise.reject(error)

export const HTTP = axios.create({
  baseURL: apiPath,
})

HTTP.interceptors.request.use(
  (config) => {
    const user = store.getState().user
    if (user.loggedIn && new Date(user.token.expiresIn) <= new Date()) {
      return axios.post(`${apiPath}/auth/refresh-token`, {
        email: user.user.email,
        refreshToken: user.token.refreshToken,
      }).then((r) => {
        store.dispatch({ type: consts.LOGIN_SUCCESS, user: null, token: r.data })
        return addAuth(config, store.getState().user)
      }).catch((error) => errorHandle(error))
    }
    return addAuth(config, user)
  },
  (error) => errorHandle(error)
)

const deepGet = (obj, props, defaultValue) => {
  if (obj === undefined || obj === null) {
    return defaultValue
  }
  if (props.length === 0) {
    return obj
  }
  const foundSoFar = obj[props[0]]
  const remainingProps = props.slice(1)
  return deepGet(foundSoFar, remainingProps, defaultValue)
}

export const obj = { deepGet }


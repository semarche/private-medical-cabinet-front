import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import { routerReducer } from 'react-router-redux'
import user from './user'
import admin from './admin'
import clients from './clients'
import manipulations from './manipulations'

export default combineReducers({
  router: routerReducer,
  form: formReducer,
  user,
  admin,
  clients,
  manipulations,
})

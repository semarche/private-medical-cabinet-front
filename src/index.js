import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'react-router-redux'
import 'sanitize.css/sanitize.css'
import 'antd/dist/antd.css'
import 'ant-design-pro/dist/ant-design-pro.css'
import store, { history } from './store'
import App from './containers/app'
import './index.css'

render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <div>
        <App />
      </div>
    </ConnectedRouter>
  </Provider>,
  document.querySelector('#root')
)
